package com.example.sudoku_csv.service;

import com.example.sudoku_csv.component.CsvOperationsComponent;
import com.example.sudoku_csv.dto.SudokuCsvDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.tags.EscapeBodyTag;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SudokuCsvServiceImpl implements SudokuCsvService{



    private CsvOperationsComponent csvOperationsComponent;

    @Autowired
    public SudokuCsvServiceImpl(CsvOperationsComponent csvOperationsComponent) {
        this.csvOperationsComponent = csvOperationsComponent;
    }


    @Override
    public Optional<SudokuCsvDto> verify() {
     Integer [][] table = csvOperationsComponent.getTable();
     SudokuCsvDto sudokuCsvDto = new SudokuCsvDto();
     boolean error = false;

     //check row ,column
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 8; j++) {
                for (int n = j + 1; n < 9; n++) {
                    if (table[i][j] == table[i][n]) {
                        sudokuCsvDto.setLineIds(i + 1);
                        error = true;
                    }
                    if (table[j][i] == table[n][i]) {
                        sudokuCsvDto.setColumnIds(i + 1);
                        error = true;
                    }
                }
            }
        }


        boolean[] bool = new boolean[9];

        int count = 1;
        for (int n = 0; n < 9; n += 3) {
            for (int m = 0; m < 9; m += 3) {
                for (int i = n; i < n + 3; i++) {
                    for (int j = m; j < m + 3; j++) {
                        if (bool[table[i][j] - 1]) {
                            sudokuCsvDto.setAreaIds(count);
                            error = true;
                        }
                        bool[(table[i][j] - 1)] = true;
                    }
                }
                count++;
                bool = new boolean[9];
            }
        }

        if(error){
            return Optional.of(sudokuCsvDto);
        }else {
            return Optional.empty();
        }

    }
}
