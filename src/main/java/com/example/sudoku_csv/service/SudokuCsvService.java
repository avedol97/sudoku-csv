package com.example.sudoku_csv.service;

import com.example.sudoku_csv.dto.SudokuCsvDto;

import java.util.Optional;

public interface SudokuCsvService {
    Optional<SudokuCsvDto> verify();
}
