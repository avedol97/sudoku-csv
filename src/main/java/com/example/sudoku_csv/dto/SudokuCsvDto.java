package com.example.sudoku_csv.dto;

import java.util.ArrayList;
import java.util.List;

public class SudokuCsvDto {
    private List<Integer> lineIds;
    private List<Integer> columnIds;
    private List<Integer> areaIds;

    public SudokuCsvDto(){
        lineIds = new ArrayList<>();
        columnIds = new ArrayList<>();
        areaIds = new ArrayList<>();
    }

    public SudokuCsvDto(List<Integer> lineIds, List<Integer> columnIds, List<Integer> areaIds) {
        this.lineIds = lineIds;
        this.columnIds = columnIds;
        this.areaIds = areaIds;
    }

    public List<Integer> getLineIds() {
        return lineIds;
    }

    public void setLineIds(Integer lineIds) {
        this.lineIds.add(lineIds);
    }

    public List<Integer> getColumnIds() {
        return columnIds;
    }

    public void setColumnIds(Integer columnIds) {
        this.columnIds.add(columnIds);
    }

    public List<Integer> getAreaIds() {
        return areaIds;
    }

    public void setAreaIds(Integer areaIds) {
        this.areaIds.add(areaIds);
    }



}
