package com.example.sudoku_csv.controller;

import com.example.sudoku_csv.dto.SudokuCsvDto;
import com.example.sudoku_csv.service.SudokuCsvServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class SudokuCsvController {

    private final SudokuCsvServiceImpl sudokuCsvService;

    public SudokuCsvController(SudokuCsvServiceImpl sudokuCsvService) {
        this.sudokuCsvService = sudokuCsvService;
    }


    @PostMapping("/api/sudoku/verify")
    public ResponseEntity<?> verify() {
        Optional<SudokuCsvDto> sudokuCsvDto = sudokuCsvService.verify();

        if (sudokuCsvDto.isPresent()) {
            return new ResponseEntity<>(sudokuCsvDto, HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);


        }

    }
}
