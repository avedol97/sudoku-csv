package com.example.sudoku_csv.component;

import java.util.List;

public interface CsvOperationsComponent {

    String PATH = "src/main/resources/sudoku_csv.csv";

    void readCsvFile();

    Integer[][] getTable();
}
