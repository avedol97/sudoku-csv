package com.example.sudoku_csv.component;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CsvOperationsComponentImpl implements CsvOperationsComponent {

    private Integer[][] table;

    public CsvOperationsComponentImpl(){
        table = new Integer[9][9];
        this.readCsvFile();
    }

    public void readCsvFile() {

        try{
            BufferedReader reader = new BufferedReader((new FileReader(PATH)));

            String line;
            reader.readLine();

            int i =0;
            while((line = reader.readLine()) != null){
               String [] c = line.split(";");
               for(int j =0;j<9;j++){
                   table[i][j] = Integer.parseInt(c[j]);
               }
               i++;
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }
    public Integer[][] getTable(){
        return table;
    }


}
