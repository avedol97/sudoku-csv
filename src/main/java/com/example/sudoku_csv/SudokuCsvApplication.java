package com.example.sudoku_csv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SudokuCsvApplication {

	public static void main(String[] args) {
		SpringApplication.run(SudokuCsvApplication.class, args);
	}

}
